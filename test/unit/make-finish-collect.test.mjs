import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import makeFinishCollect from '../../src/make-finish-collect.mjs';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const next = sandbox.stub();

describe('src/make-finish-collect', function () {
    afterEach(() => sandbox.reset());

    it('Should yield the collections map', () => {
        const collectionsMap = {emptyCollection: []};
        const subject = makeFinishCollect(collectionsMap);
        subject(next);
        expect(next).to.be.calledOnceWithExactly(null, collectionsMap);
    });
});