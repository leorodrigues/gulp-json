import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeYieldCollectionsApiCall from '../../src/make-yield-collections-api-call.mjs';
import makeLogger from '@leorodrigues/logging';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const logger = makeLogger('unit-test');

describe('src/make-yield-collections-api-call', function () {
    afterEach(() => sandbox.reset());

    it('Should produce a handler factory which in turn produces a readable stream', () => {
        const collectionsMap = { items: [] };
        const yieldCollections = makeYieldCollectionsApiCall(collectionsMap, logger);

        const stream = yieldCollections();
        const result = stream.forEach(o =>
            expect(result).to.be.deep.equal(o));
    });
});
