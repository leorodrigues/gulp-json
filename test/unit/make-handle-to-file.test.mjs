import path from 'path';
import { Readable } from 'stream';

import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeLogger from '@leorodrigues/logging';

import makeHandleToFile from '../../src/make-handle-to-file.mjs';
import PluginError from 'plugin-error';
import Vinyl from 'vinyl';

chai.use(sinonChai);

const logger = makeLogger('unit-test');

const { expect } = chai;
const sandbox = sinon.createSandbox();

const next = sandbox.stub();

const testOptions = { base: path.join('output', 'tests'), extension: '.json' };

const nullVinyl = new Vinyl({ contents: null });
const streamVinyl = new Vinyl({ contents: new Readable() });
const goodVinyl = new Vinyl({
    contents: Buffer.from('hello world'),
    cwd: path.resolve('.'),
    path: path.join('some', 'path', 'message.txt'),
    base: path.join('some', 'path')
});

describe('src/make-handle-to-file', function () {
    afterEach(() => sandbox.reset());

    it('Should yield a new vinyl if the input is something else', () => {
        const subject = makeHandleToFile(testOptions, logger);
        subject({ }, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(null, sinon.match.instanceOf(Vinyl));
    });

    it('Should yield the vinyl forward if it is null', () => {

        const subject = makeHandleToFile(testOptions, logger);
        subject(nullVinyl, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(null, nullVinyl);
    });

    it('Should yield an exception if the vinyl is stream', () => {
        const subject = makeHandleToFile(testOptions, logger);
        subject(streamVinyl, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(sinon.match.instanceOf(PluginError));
    });

    it('Should yield a new vinyl based on the previous one', () => {
        const subject = makeHandleToFile(testOptions, logger);
        subject(goodVinyl, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(null, sinon.match.instanceOf(Vinyl));
    });
});