import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeParseApiCall from '../../src/make-parse-api-call.mjs';
import { PassThrough, Writable } from 'stream';
import makeLogger from '@leorodrigues/logging';

const logger = makeLogger('test-logger');

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const makeHandleParse = sandbox.stub();

describe('src/make-parse-api-call', function () {
    afterEach(() => sandbox.reset());

    it('Should produce a handler factory which in turn produces a transform object', () => {
        const write = sandbox.stub();
        const final = n => n();
        const handle = (o, e, n) => n(null, o);

        makeHandleParse.returns(handle);

        const parse = makeParseApiCall(logger, makeHandleParse);

        const stream = new PassThrough({ objectMode: true });
        stream.pipe(parse()).pipe(new Writable({
            write: write, final: final, objectMode: true
        }));

        stream.push({message: 'hello test'});

        expect(write).to.be.calledWith({message: 'hello test'});
    });
});
