import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import makeHandleCollect from '../../src/make-handle-collect.mjs';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const next = sandbox.stub();

describe('src/make-handle-collect', function () {
    afterEach(() => sandbox.reset());

    it('Should apped to the collection and pass the object forward', () => {
        const collection = [];
        const obj = {};
        const subject = makeHandleCollect(collection);
        subject(obj, 'utf-8', next);
        expect(collection).to.be.deep.equal([obj]);
        expect(next).to.be.calledOnceWithExactly(null, obj);
    });
});