import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeLogger from '@leorodrigues/logging';
import makePluginFactory from '../../src/make-plugin-factory.mjs';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();

const makeToFileApiCall = sandbox.stub();
const makeParseApiCall = sandbox.stub();
const makeCollectApiCall = sandbox.stub();
const makeYieldCollectionsApiCall = sandbox.stub();

const logger = makeLogger('unit-test');

describe('src/make-plugin-factory', function () {
    afterEach(() => sandbox.reset());

    it('Should assemble the plugin instance', () => {
        makeToFileApiCall.returns('fake api call');
        makeParseApiCall.returns('fake api call');
        makeCollectApiCall.returns('fake api call');
        makeYieldCollectionsApiCall.returns('fake api call');
        const options = {
            prettyPrint: false, extension: '.json', message: 'whaterver'
        };

        const makeGulpJson = makePluginFactory(
            'fake makeHandleToFile function',
            'fake makeHandleParse function',
            'fake makeHandleCollect function',
            'fake makeFinishCollect function',
            makeToFileApiCall,
            makeParseApiCall,
            makeCollectApiCall,
            makeYieldCollectionsApiCall,
            logger
        );

        const instance = makeGulpJson({ message: 'whaterver' });

        expect(makeToFileApiCall).to.be.calledOnceWithExactly(
            options, logger, 'fake makeHandleToFile function');

        expect(makeParseApiCall)
            .to.be.calledOnceWithExactly(logger, 'fake makeHandleParse function');

        expect(makeCollectApiCall).to.be.calledOnceWithExactly(
            { }, 'fake makeHandleCollect function', 'fake makeFinishCollect function');

        expect(makeYieldCollectionsApiCall)
            .to.be.calledOnceWithExactly({ }, logger);

        expect(instance).to.be.deep.equal({
            toFile: 'fake api call',
            parse: 'fake api call',
            collect: 'fake api call',
            yieldCollections: 'fake api call',
            logger
        });
    });
});
