import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeLogger from '@leorodrigues/logging';

import makeHandleParse from '../../src/make-handle-parse.mjs';
import PluginError from 'plugin-error';

chai.use(sinonChai);

const logger = makeLogger('unit-test');

const { expect } = chai;
const sandbox = sinon.createSandbox();

const next = sandbox.stub();
const fakeVinyl = {
    isNull: sandbox.stub(),
    isStream: sandbox.stub(),
    relative: 'some/fake/path',
    contents: '{"number":112358}'
};

describe('src/make-handle-parse', function () {
    afterEach(() => sandbox.reset());

    it('Should yield an empty object if the vinyl is null', () => {
        fakeVinyl.isNull.returns(true);

        const subject = makeHandleParse(logger);
        subject(fakeVinyl, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(null, {});
    });

    it('Should yield an exception if the vinyl is stream', () => {
        fakeVinyl.isStream.returns(true);

        const subject = makeHandleParse(logger);
        subject(fakeVinyl, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(sinon.match.instanceOf(PluginError));
    });

    it('Should yield a parsed json', () => {
        const subject = makeHandleParse(logger);
        subject(fakeVinyl, 'utf-8', next);

        expect(next).to.be.calledOnceWithExactly(null, {number: 112358});
    });
});