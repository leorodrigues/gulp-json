import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeToFileApiCall from '../../src/make-to-file-api-call.mjs';
import { PassThrough, Writable } from 'stream';
import makeLogger from '@leorodrigues/logging';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const makeHandleToFile = sandbox.stub();

const logger = makeLogger('test-logger');

describe('src/make-to-file-api-call', function () {
    afterEach(() => sandbox.reset());

    it('Should merge base and local options', () => {
        makeHandleToFile.returns(() => {});
        const toFile = makeToFileApiCall({ messagePart1: 'hello' }, logger, makeHandleToFile);
        toFile({ messagePart2: 'world' });

        expect(makeHandleToFile).to.be.calledOnceWithExactly({
            messagePart1: 'hello',
            messagePart2: 'world'
        }, logger);
    });

    it('Should produce a handler factory which in turn produces a transform object', () => {
        const write = sandbox.stub();
        const final = n => n();
        const handle = (o, e, n) => n(null, o);

        makeHandleToFile.returns(handle);

        const toFile = makeToFileApiCall({}, logger, makeHandleToFile);

        const stream = new PassThrough({ objectMode: true });
        stream.pipe(toFile()).pipe(new Writable({
            write: write, final: final, objectMode: true
        }));

        stream.push({message: 'hello test'});

        expect(write).to.be.calledWith({message: 'hello test'});
    });
});
