import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import makeCollectApiCall from '../../src/make-collect-api-call.mjs';
import { PassThrough, Writable } from 'stream';

chai.use(sinonChai);

const { expect } = chai;

const sandbox = sinon.createSandbox();
const makeHandleCollect = sandbox.stub();
const makeFinishCollect = sandbox.stub();

describe('src/make-collect-api-call', function () {
    afterEach(() => sandbox.reset());

    it('Should create a collection if it does not exist on the map', () => {
        makeHandleCollect.returns(() => {});
        makeFinishCollect.returns(() => {});

        const collectionsMap = {};

        const subject = makeCollectApiCall(
            collectionsMap, makeHandleCollect, makeFinishCollect);

        subject('someCollection');

        expect(collectionsMap).to.be.deep.equal({ someCollection: [ ] });
        expect(makeHandleCollect).to.be.calledOnceWithExactly([]);
        expect(makeFinishCollect).to.be.calledOnceWithExactly(collectionsMap);
    });

    it('Should produce a handler factory which in turn produces a transform object', () => {
        const collectionsMap = {};
        const write = sandbox.stub();

        const final = n => n();
        const handle = (o, e, n) => n(null, o);
        const finish = n => n(collectionsMap);

        makeHandleCollect.returns(handle);
        makeFinishCollect.returns(finish);

        const collect = makeCollectApiCall(
            collectionsMap, makeHandleCollect, makeFinishCollect);

        const stream = new PassThrough({ objectMode: true });
        stream.pipe(collect('testCollection'))
            .pipe(new Writable({ write: write, final: final, objectMode: true }));

        stream.push({message: 'hello test'});

        expect(write).to.be.calledWith({message: 'hello test'});
    });
});
