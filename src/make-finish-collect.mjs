
export default function makeFinishCollect(collections) {
    return function finishCollect(next) {
        next(null, collections);
    };
}
