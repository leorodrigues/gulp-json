
export default function makeHandleCollect(collection) {
    return function collect(object, encoding, next) {
        collection.push(object);
        next(null, object);
    };
}