import Vinyl from 'vinyl';
import PluginError from 'plugin-error';
import uniqueString from 'unique-string';

function defaultPath(object) {
    return `${object.id || uniqueString()}.json`;
}

function replaceExtension(path, extension) {
    return path.replace(/\.\w+$/, extension);
}

function resolvePath(object, path) {
    let result = path || object.path || defaultPath;

    if (result instanceof Function)
        result = result(object) || defaultPath(object);

    return result;
}

function resolveFileParams(object, options) {
    const indent = options.prettyPrint ? 4 : 0;
    let { base, path } = options;
    const { attributeName, extension } = options;
    const content = object[attributeName] || object;

    path = resolvePath(object, path);
    path = replaceExtension(path, extension);

    const cwd = object.cwd || process.cwd();
    base = base || object.base;

    return {content, cwd, base, path, indent};
}

function makeVinyl(object, cwd, base, path, encoding, indent) {
    const newFile = new Vinyl({cwd, base, path});

    if (object) {
        const jsonString = JSON.stringify(object, encoding, indent);
        newFile.contents = Buffer.from(jsonString);
    }

    return newFile;
}

function handleVinyl(file, encoding, next, logger, options) {
    if (file.isNull())
        return next(null, file);

    if (file.isStream())
        return next(new PluginError('gulp-json', 'Streams not supported'));

    const {content, cwd, base, path, indent} =
        resolveFileParams(file, options);

    const newFile = makeVinyl(content, cwd, base, path, encoding, indent);

    logger.info('Vinyl instantiated.', 'path', newFile.relative);

    next(null, newFile);
}

function handleJsonObject(object, encoding, next, logger, options) {
    const {content, cwd, base, path, indent} =
        resolveFileParams(object, options);

    const newFile = makeVinyl(content, cwd, base, path, encoding, indent);

    logger.info('Vinyl instantiated.', 'path', newFile.relative);

    next(null, newFile);
}

export default function makeHandleToFile(options, logger) {
    return function handleToFile(object, encoding, next) {
        if (Vinyl.isVinyl(object))
            handleVinyl(object, encoding, next, logger, options);
        else
            handleJsonObject(object, encoding, next, logger, options);
    };
}
