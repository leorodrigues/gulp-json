import { Transform } from 'stream';

export default function makeToFileApiCall(
    baseOptions, logger, makeHandleToFile
) {
    return function toFile(localOptions = { }) {
        const options = { ...baseOptions, ...localOptions };
        const handler = makeHandleToFile(options, logger);
        return new Transform({ objectMode: true, transform: handler });
    };
}
