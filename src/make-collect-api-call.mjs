import { Transform } from 'stream';

function getCollection(collections, name) {
    return collections[name] || (collections[name] = []);
}

export default function makeCollectApiCall(
    collections, makeHandleCollect, makeFinishCollect
) {
    return function collect(collectionName) {
        const name = collectionName || 'items';
        const collection = getCollection(collections, name);

        return new Transform({
            objectMode: true,
            transform: makeHandleCollect(collection),
            flush: makeFinishCollect(collections)
        });
    };
}
