import { Readable } from 'stream';

export default function makeYieldCollectionsApiCall(collectionsMap, logger) {
    return function yieldCollections() {
        const names = Object.keys(collectionsMap);
        const stats = names.flatMap(n =>
            [`${n}Size`, collectionsMap[n].length]
        );

        logger.info('Collections ready.', 'names', names, ...stats);

        const source = [collectionsMap];
        return new Readable({
            objectMode: true,
            read() { this.push(source.shift() || null); }
        });
    };
}
