
const DEFAULT_OPTIONS = {
    prettyPrint: false,
    extension: '.json'
};

export default function makePluginFactory(
    makeHandleToFile,
    makeHandleParse,
    makeHandleCollect,
    makeFinishCollect,
    makeToFileApiCall,
    makeParseApiCall,
    makeCollectApiCall,
    makeYieldCollectionsApiCall,
    logger
) {
    return function makeGulpJson(options = { }) {
        const collections = { };
        const baseOptions = {...DEFAULT_OPTIONS, ...options};

        const toFile = makeToFileApiCall(baseOptions, logger, makeHandleToFile);

        const parse = makeParseApiCall(logger, makeHandleParse);

        const collect = makeCollectApiCall(
            collections, makeHandleCollect, makeFinishCollect);

        const yieldCollections =
            makeYieldCollectionsApiCall(collections, logger);

        const instance = { toFile, parse, collect, yieldCollections, logger };

        logger.info('Plugin instance assembled.');

        return instance;
    };
}
