import { Transform } from 'stream';

export default function makeParseApiCall(logger, makeHandleParse) {
    return function parse() {
        return new Transform({
            objectMode: true,
            transform: makeHandleParse(logger)
        });
    };
}
