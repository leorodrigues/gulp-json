import PluginError from 'plugin-error';

export default function makeHandleParse(logger) {
    return function parse(file, encoding, next) {
        if (file.isNull())
            return next(null, {});

        if (file.isStream())
            return next(new PluginError('gulp-json', 'Streams not supported'));

        const object = JSON.parse(file.contents);

        logger.info('JSON file parsed.', 'path', file.relative);

        next(null, object);
    };
}
