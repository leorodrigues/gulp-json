import makeLogger from '@leorodrigues/logging';

import makeHandleCollect from './src/make-handle-collect.mjs';
import makeFinishCollect from './src/make-finish-collect.mjs';
import makeHandleParse from './src/make-handle-parse.mjs';
import makeHandleToFile from './src/make-handle-to-file.mjs';

import makeCollectApiCall from './src/make-collect-api-call.mjs';
import makeParseApiCall from './src/make-parse-api-call.mjs';
import makePluginFactory from './src/make-plugin-factory.mjs';
import makeToFileApiCall from './src/make-to-file-api-call.mjs';
import makeYieldCollectionsApiCall
    from './src/make-yield-collections-api-call.mjs';

const PLUGIN_NAME = 'gulp-json';

const logger = makeLogger(PLUGIN_NAME);

const makeGulpJson = makePluginFactory(
    makeHandleToFile,
    makeHandleParse,
    makeHandleCollect,
    makeFinishCollect,
    makeToFileApiCall,
    makeParseApiCall,
    makeCollectApiCall,
    makeYieldCollectionsApiCall,
    logger,
);

export default makeGulpJson;